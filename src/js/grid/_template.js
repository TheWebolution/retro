export default `<table class="retro__grid" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th v-bind:style="styleObject"></th>
            <th v-for="col in grid.cols" v-bind:style="styleObject" v-bind:class="{ highlighted: shouldHighlight(col, 0) }">{{ col }}</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="row in grid.rows">
            <td v-bind:style="styleObject" v-bind:class="{ highlighted: shouldHighlight(0, row) }">{{ row }}</th>
            <td
                v-for="col in grid.cols"
                v-bind:style="styleObject"
                :row="row"
                :col="col"
                v-on:drop="moveEntity(col, row)"
                v-on:dragover="highlightCells(col, row)"
                v-bind:class="{ highlighted: shouldHighlight(col, row) }"
            ></td>
        </tr>
    </tbody>
</table>`

// export default `
//     <div class="retro__grid">
//         <ul class="retro__grid__heading retro__grid__heading--cols">
//             <li></li>
//             <li v-for="col in grid.cols" v-bind:style="styleObject" v-bind:class="{ highlighted: shouldHighlight(col, 0) }">{{ col }}</li>
//         </ul>

//         <ul class="retro__grid__heading retro__grid__heading--rows">
//             <li></li>
//             <li v-for="row in grid.rows" v-bind:style="styleObject" v-bind:class="{ highlighted: shouldHighlight(0, row) }">{{ row }}</li>
//         </ul>

//         <div class="retro__grid__wrapper">
//             <table class="retro__grid__table">
//                 <tbody>
//                     <tr v-for="row in grid.rows">
//                         <td
//                             v-for="col in grid.cols"
//                             v-bind:style="styleObject"
//                             :row="row"
//                             :col="col"
//                             v-on:drop="moveEntity(col, row)"
//                             v-on:dragover="highlightCells(col, row)"
//                             v-bind:class="{ highlighted: shouldHighlight(col, row) }"
//                         ></td>
//                     </tr>
//                 </tbody>
//             </table>
//         </div>
//     </div>
// `