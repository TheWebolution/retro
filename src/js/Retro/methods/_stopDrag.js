export default function() {
    this.$children.find(entity => {
        return entity.highlight
    }).highlight = { x: null, y: null }
}