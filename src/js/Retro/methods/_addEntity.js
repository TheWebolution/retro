export default function(entity) {
    const type = this.types.find(type => {
        return type.name === entity.type
    })

    for (let i = 1; i <= entity.quantity; i++) {
        this.entities.push(Object.assign({}, {
            x: i,
            y: -1,
            name: entity.name,
            type: entity.type,
            selected: false,
            moving: false,
            maxHealth: type.health,
            health: type.health,
            type: entity.type,
            highlight: {
                x: null,
                y: null
            }
        }))
    }
}