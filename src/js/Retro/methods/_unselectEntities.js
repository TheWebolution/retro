export default function () {
    let selectedEntity = this.entities.find(entity => {
        return entity.selected
    })

    if(selectedEntity) {
        selectedEntity.selected = false
    }
}