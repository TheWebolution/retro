export default function(coords) {
    let entity = this.entities.find(entity => {
        return entity.moving === true
    })
    entity.moving = false
    entity.x = coords.x
    entity.y = coords.y
}