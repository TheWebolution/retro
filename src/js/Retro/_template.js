export default `
    <div class="retro">
        <retro-grid
            v-bind:grid="grid"
            @dropOn="dropEntity"
        >
            <slot></slot>
        </retro-grid>
        
        <retro-entity
            v-for="(entity, index) in entities"
            :key="index"
            v-bind:index="index"
            v-bind:entity="entity"
            v-bind:size="grid.size"
            @stopDrag="stopDrag"
            @selectEntity="unselectEntities"
        >
            <slot></slot>
        </retro-entity>

        <div v-bind:class="{ open: areSettingsOpen, retro__settings: true }">
            <h4>Add entities</h4>
            
            <div class="retro__settings__entities">
                <p class="retro__settings__entities__option">Name</p>
                <input v-model="entity.name" type="text" />
            </div>
            <div class="retro__settings__entities">
                <p class="retro__settings__entities__option">Type</p>
                <select v-model="entity.type">
                    <option
                        v-for="type in types"
                        :value="type.name"
                    >{{ type.title }}</option>
                </select>
            </div>
            <div class="retro__settings__entities">
                <p class="retro__settings__entities__option">Quantity</p>
                <span class="btn" v-on:click="() => { entity.quantity <= 1 || entity.quantity-- }"><i class="fas fa-minus"></i></span>
                <input v-model.number="entity.quantity" type="number" placeholder="Quantity" class="hidden" />
                <p class="retro__settings__entities__value">{{ entity.quantity }}</p>
                <span class="btn" v-on:click="() => { entity.quantity++ }"><i class="fas fa-plus"></i></span>
            </div>
            <span v-on:click="addEntity(entity)" class="retro__settings__entities__add btn">Add</span>

            <h4>Grid settings</h4>
            <div class="retro__settings__grid">
                <p class="retro__settings__grid__option">Columns</p>
                <span class="btn" v-on:click="() => { grid.cols <= 1 || grid.cols-- }"><i class="fas fa-minus"></i></span>
                <p class="retro__settings__grid__value">{{ grid.cols }}</p>
                <span class="btn" v-on:click="() => { grid.cols++ }"><i class="fas fa-plus"></i></span>
            </div>
            <div class="retro__settings__grid">
                <p class="retro__settings__grid__option">Rows</p>
                <span class="btn" v-on:click="() => { grid.rows <= 1 || grid.rows-- }"><i class="fas fa-minus"></i></span>
                <p class="retro__settings__grid__value">{{ grid.rows }}</p>
                <span class="btn" v-on:click="() => { grid.rows++ }"><i class="fas fa-plus"></i></span>
            </div>
            <div class="retro__settings__grid">
                <p class="retro__settings__grid__option">Cells' size</p>
                <span class="btn" v-on:click="() => { grid.size <= 1 || grid.size-- }"><i class="fas fa-minus"></i></span>
                <p class="retro__settings__grid__value">{{ grid.size }}</p>
                <span class="btn" v-on:click="() => { grid.size++ }"><i class="fas fa-plus"></i></span>
            </div>

            <span class="retro__settings__toggle btn" v-on:click="toggleSettings">
                <i
                    v-bind:class="{
                        'fas': true,
                        'fa-bars': !areSettingsOpen,
                        'fa-times': areSettingsOpen
                    }"
                ></i>
            </span>
        </div>
    </div>
`