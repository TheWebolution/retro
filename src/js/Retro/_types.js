export default [
    {
        name: 'fighter',
        title: 'Developer',
        health: 5
    },
    {
        name: 'cleric',
        title: 'Test Analyst',
        health: 3
    },
    {
        name: 'wizard',
        title: 'Wizard',
        health: 3
    },
    {
        name: 'bard',
        title: 'Bard',
        health: 3
    },
    {
        name: 'bug',
        title: 'Bug',
        health: 1
    },
    {
        name: 'zombie',
        title: 'Zombie',
        health: 3
    },
    {
        name: 'bigZombie',
        title: 'Big Zombie',
        health: 8
    },
    {
        name: 'mdn',
        title: 'Makeeneno D\'Nat Hal',
        health: 12
    },
    {
        name: 'boss',
        title: 'Project Boss',
        health: 20
     }
]