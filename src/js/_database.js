import firebase from 'firebase'

const config = {
    apiKey: "AIzaSyAPsghgLhQDv7RfFUwWv3YYjYAgHz1kGm0",
    authDomain: "retro-tool-16232.firebaseapp.com",
    databaseURL: "https://retro-tool-16232.firebaseio.com",
    projectId: "retro-tool-16232",
    storageBucket: "retro-tool-16232.appspot.com",
    messagingSenderId: "636173885628"
}

firebase.initializeApp(config)
let database = firebase.database()

export default database