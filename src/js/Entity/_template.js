export default /*html*/`
    <div
        v-bind:style="entityStyle"
        v-on:drag="dragEntity"
        v-on:dragend="stopDrag"
        v-bind:class="{
            selected: entity.selected,
            moving: entity.moving,
            retro__entity: true
        }"
    >
        <div class="retro__entity__image" draggable="true" v-bind:style="dragAreaStyle">
            <img
                v-bind:src="imagePath"
                v-on:click="toggleEntity"
            />
        </div>
        
        <div
            class="retro__entity__health"
            v-bind:style="healthStyle"
        >
        </div>
        
        <div class="retro__entity__name">{{ entity.name }}</div>
        <div class="retro__entity__edit">
            <span v-on:click="hitEntity" class="btn">
                <img src="img/hit.png" />
            </span>
            <span v-on:click="healEntity" class="btn">
                <img src="img/heal.png" />
            </span>
            <span v-on:click="killEntity" class="btn">
                <img src="img/kill.png" />
            </span>
        </div>
        <div class="retro__entity__area retro__entity__area--move" v-bind:style="getAreaStyle(5)"></div>
        <div class="retro__entity__area retro__entity__area--action" v-bind:style="getAreaStyle(2)"></div>
    </div>
`