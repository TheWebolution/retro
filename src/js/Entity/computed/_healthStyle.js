export default function() {
    const health = this.entity.health * 100 / this.entity.maxHealth
    const background = health > 60 ? '28A605' : health > 30 ? 'F2A007' : 'A60A0A'
    return {
        'width': `${health}%`,
        'background': `#${background}`
    }
}