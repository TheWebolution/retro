export default function() {
    return {
        'top': `${this.size * this.entity.y}px`,
        'left': `${(this.size + 1) * this.entity.x}px`,
        'height': `${this.size}px`,
        'width': `${this.size}px`
    }
}