export default function() {
    return {
        'height': `${this.size}px`,
        'width': `${this.size}px`
    }
}