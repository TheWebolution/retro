export default function() {
    const wasSelected = this.entity.selected
    this.$emit('selectEntity')
    this.rootEntity.selected = !wasSelected
}