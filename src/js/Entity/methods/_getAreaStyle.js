export default function(range) {
    if (this.entity.x <= 0 || this.entity.y <= 0) {
        return
    }

    const areaRange = (range * 2) + 1
    const entityRange = range + 1

    const topDistance = entityRange - this.entity.y
    const leftDistance = entityRange - this.entity.x

    const heightCells = topDistance < 0 ? areaRange : areaRange - topDistance
    const widthCells = leftDistance < 0 ? areaRange : areaRange - leftDistance
    const topCells = entityRange - heightCells
    const leftCells = entityRange - widthCells

    console.log(widthCells, heightCells)

    return this.entity.selected ? {
        'height': `${(this.size * heightCells) - entityRange}px`,
        'width': `${(this.size * widthCells) + (widthCells - range)}px`,
        'top': `${this.size * topCells}px`,
        'left': `${(this.size * leftCells) - (widthCells - range)}px`,
    } : null
}