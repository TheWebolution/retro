'use strict';
const gulp = require('gulp');
const sass = require('gulp-sass');
const webpack = require('gulp-webpack');

gulp.task('build:sass', function () {
    return gulp.src('./src/sass/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./'));
});

gulp.task('build:js', function () {
    return gulp.src('./src/js/index.js')
        .pipe(webpack({
            entry: {
                index: './src/js/index.js'
            },
            output: {
                filename: '[name].js',
            },
            module: {
                loaders: [
                    {
                        test: /\.js$/,
                        loader: 'babel-loader',
                        query: {
                            presets: ['env']
                        }
                    }
                ]
            },
            resolve: {
                alias: {
                    vue: 'vue/dist/vue.js'
                }
            }
        }))
        .pipe(gulp.dest('./'));
});

gulp.task('watch:sass', function () {
    gulp.watch('./src/sass/**/*.scss', ['build:sass']);
});

gulp.task('watch:js', function () {
    gulp.watch('./src/js/**/*.js', ['build:js']);
});

gulp.task('build', ['build:sass', 'build:js']);

gulp.task('watch', ['build', 'watch:js', 'watch:sass']);